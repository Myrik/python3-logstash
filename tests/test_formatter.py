import json
import logging
import os
import unittest
import uuid

from freezegun import freeze_time

from logstash_sync import LogstashFormatterVersion0, LogstashFormatterVersion1


class TestLiveKAuction(unittest.TestCase):
    def get_log_record(self):
        record = logging.LogRecord(
            "name",
            logging.ERROR,
            __file__,
            0,
            "message",
            (),
            exc_info=None,
            func="test_formatter_0",
        )

        record.dict = {1: 1}
        record.int = 1
        record.str = "str"
        record.set = {1, 2, 3}
        record.uuid = uuid.UUID("1cde2f7c-dbf4-4482-8dae-364c9fb6a834")

        return record

    @freeze_time("09-10-2018 23:22:21")
    def test_formatter_0(self):
        record = self.get_log_record()

        formatter = LogstashFormatterVersion0()
        result = formatter.format(record)

        self.assertEqual(
            json.loads(result),
            {
                "@timestamp": "2018-09-10T23:22:21.000Z",
                "@message": "message",
                "@source": f"Logstash://{os.uname()[1]}/{os.path.abspath(os.curdir)}/tests/test_formatter.py",
                "@source_host": os.uname()[1],
                "@source_path": f"{os.path.abspath(os.curdir)}/tests/test_formatter.py",
                "@tags": [],
                "@type": "Logstash",
                "@fields": {
                    "class_name": "tests.test_formatter.TestLiveKAuction",
                    "dict": {"1": 1},
                    "int": 1,
                    "levelname": "ERROR",
                    "logger": "name",
                    "set": [1, 2, 3],
                    "stack_info": None,
                    "str": "str",
                    "uuid": "1cde2f7c-dbf4-4482-8dae-364c9fb6a834",
                },
            },
        )

    @freeze_time("09-10-2018 23:22:21")
    def test_formatter_1(self):
        record = self.get_log_record()

        formatter = LogstashFormatterVersion1()
        result = formatter.format(record)

        self.assertEqual(
            json.loads(result),
            {
                "@timestamp": "2018-09-10T23:22:21.000Z",
                "message": "message",
                "host": os.uname()[1],
                "path": f"{os.path.abspath(os.curdir)}/tests/test_formatter.py",
                "tags": [],
                "type": "Logstash",
                "level": "ERROR",
                "logger_name": "name",
                "stack_info": None,
                "class_name": "tests.test_formatter.TestLiveKAuction",
                "dict": {"1": 1},
                "int": 1,
                "set": [1, 2, 3],
                "str": "str",
                "uuid": "1cde2f7c-dbf4-4482-8dae-364c9fb6a834",
            },
        )
